/*
 * MUSTARD: Android's Client for StatusNet
 * 
 * Copyright (C) 2009-2010 macno.org, Michele Azzolari
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

package org.mustard.android.activity;

import java.net.MalformedURLException;
import java.net.URL;

import oauth.signpost.exception.OAuthNotAuthorizedException;

import org.mustard.android.MustardApplication;
import org.mustard.android.MustardDbAdapter;
import org.mustard.android.R;
import org.mustard.android.core.OAuthKeyFetcher;
import org.mustard.android.provider.OAuthInstance;
import org.mustard.android.provider.OAuthLoader;
import org.mustard.android.provider.StatusNet;
import org.mustard.oauth.OAuthManager;
import org.mustard.statusnet.StatusNetService;
import org.mustard.util.MustardException;
import org.mustard.util.MustardOAuthException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class OAuthLogin extends Activity {

	private String TAG = getClass().getCanonicalName();
	
	protected static String EXTRA_INSTANCE = "extra.instance";
	
	private EditText mInstanceEdit;
	private CheckBox mForceSSLEdit;
	private Button mOAuthButton;
	private Button mLoginButton;
	
	private String mInstance;
	private MustardDbAdapter mDbHelper; 
	private String mSURL;
	private URL mURL;

	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.oauthfirststep);
		setTitle(getString(R.string.app_name)+" - " + getString(R.string.lbl_create_account));
		mDbHelper = new MustardDbAdapter(this);
		mDbHelper.open();
		mInstanceEdit = (EditText) findViewById(R.id.edit_instance);
		mInstanceEdit.setText("");
		mForceSSLEdit = (CheckBox) findViewById(R.id.force_ssl);
		
		mOAuthButton = (Button) findViewById(R.id.button_oauth);
		mOAuthButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				doOAuthLogin();
			}
		});

		mLoginButton = (Button) findViewById(R.id.button_login);
		mLoginButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				doLogin();
			}
			
		});

		Intent intent = getIntent();

		String instance = intent.getStringExtra(EXTRA_INSTANCE);
		if (instance != null && !"".equals(instance)) {
			mInstanceEdit.setText(instance);
			// If I have the instance I force the 
			doOAuthLogin();
		}
		
	}
	
	private void doLogin() {
		BasicAuthLogin.actionHandleLogin(this);
		finish();
	}
	
	public void doOAuthLogin() {
		try {
			// I force a keys download.. 
			OAuthKeyFetcher oaf = new OAuthKeyFetcher();
			oaf.execute(this, mDbHelper, null);
			requestToken();
		} catch (MustardOAuthException e) {
			new AlertDialog.Builder(OAuthLogin.this)
            .setTitle(getString(R.string.error))
            .setMessage(e.getMessage())
            .setNeutralButton(getString(R.string.close), null).show();
		} catch (MalformedURLException e) {
			new AlertDialog.Builder(OAuthLogin.this)
            .setTitle(getString(R.string.error))
            .setMessage(getString(R.string.error_invalid_url))
            .setNeutralButton(getString(R.string.close), null).show();
		} catch (OAuthNotAuthorizedException e) {
			new AlertDialog.Builder(OAuthLogin.this)
            .setTitle(getString(R.string.error))
            .setMessage(R.string.oauth_wrong_consumer_key)
            .setNeutralButton(getString(R.string.close), null).show();
		} catch (Exception e) {
			e.printStackTrace();
			new AlertDialog.Builder(OAuthLogin.this)
            .setTitle(getString(R.string.error))
            .setMessage(getString(R.string.error_generic_detail,e.getMessage()==null ? e.toString() : e.getMessage()))
            .setNeutralButton(getString(R.string.close), null).show();
		}
	}
	
	public void requestToken() throws Exception {
		
		mInstance = mInstanceEdit.getText().toString();
		if (mInstance == null || "".equals(mInstance))
			return;
		mInstance=mInstance.toLowerCase();
		if (mInstance.endsWith("/"))
			mInstance=mInstance.substring(0, mInstance.length()-1);
	
		boolean isTwitter = false;
		String instance="";
		if (mInstance.toLowerCase().indexOf("twitter.com")>=0) {
			mForceSSLEdit.setChecked(true);
			mInstance="twitter.com";
			instance=mInstance;
			mSURL = "https://" + mInstance;
			mURL = new URL(mSURL);
			isTwitter=true;
		} else {
			if (mInstance.toLowerCase().startsWith("http"))
				mSURL = mInstance;
			else {
				if(mForceSSLEdit.isChecked())
					mSURL = "https://" + mInstance;
				else
					mSURL = "http://" + mInstance;
			}
		
			try {
				mURL = new URL(mSURL);
			} catch (MalformedURLException e) {
				throw e;
			}

			instance = mSURL.startsWith("https") ? mSURL.substring(8) : mSURL.substring(7);

			try {
				isStatusNetInstance();
			} catch (MustardException e) {
				throw e;
			}
		
		}
//	    Log.i(getPackageName(),"mSURL = " + mSURL);
//	    Log.i(getPackageName(),"instance = " + instance);
	   
	    OAuthLoader om = new OAuthLoader(mDbHelper) ;
	    OAuthInstance oi =  om.get(instance);
	    if (oi == null )  {
	    	throw new MustardOAuthException(getString(R.string.error_no_oauth,mInstance));
	    }
	    
//        Log.i(getPackageName(),"key: " + oi.key);
//        Log.i(getPackageName(),"key secret: " + oi.secret);
        
	    OAuthManager oauthManager = OAuthManager.getOAuthManager();
	    oauthManager.prepare(
	    		oi.key,
	            oi.secret,
        		mSURL + (!isTwitter ? "/api" : "") + "/oauth/request_token",
        		mSURL + (!isTwitter ? "/api" : "") + "/oauth/access_token",
        		mSURL + (!isTwitter ? "/api" : "") + "/oauth/authorize");

        String authUrl = null;
        authUrl = oauthManager.retrieveRequestToken("statusnet://oauth");
        
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String cToken = oauthManager.getConsumer().getToken();
        String cTokenSecret = oauthManager.getConsumer().getTokenSecret();
//        Log.d("Mustard", "Token -> " + cToken);
        mSharedPreferences.edit().putString("Request_token", cToken)
        	.putString("Request_token_secret", cTokenSecret)
        	.putString("oauth_url",mSURL)
        	.putBoolean("is_twitter",isTwitter)
        	.putBoolean("oauth_10a",oauthManager.isOAuth10a())
        	.putString("instance",instance)
        	.commit();
        if(authUrl!=null && !"".equals(authUrl)) {
        	startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(authUrl)));
        	setResult(RESULT_OK);
        	finish();
        } else {
        	throw new Exception("authURL is null :/");
        }
	}
	
	private String isStatusNetInstance() throws MustardException {
		StatusNet mStatusNet = new StatusNet(this);
		mStatusNet.setURL(mURL);
		
		StatusNetService sns = null;
		try {
			// Check if mURL is a statusnet instance
			sns = mStatusNet.getConfiguration();
		} catch (MustardException e) {
//			if(MustardApplication.DEBUG)
//				e.printStackTrace();
			throw new MustardException(getString(R.string.error_help_test));
		} catch (Exception e) {
			throw new MustardException(getString(R.string.error_help_test));
		}
		if (sns == null) {
			// this is not a SN instance..
			throw new MustardException(getString(R.string.error_help_test));
		}		
		return "";
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(MustardApplication.DEBUG) Log.i(TAG, "onDestroy");
		if(mDbHelper != null) {
			try {
				mDbHelper.close();
			} catch (Exception e) {
				if (MustardApplication.DEBUG) e.printStackTrace();
			}
		}
	}
	
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.account_setup_oauth_option, menu);
        return true;
    }
    
    private void onOAuthSetup() {
    	OAuthSettings.actionOAuthSettings(this);
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.oauth_settings:
            	onOAuthSetup();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    
	public static void actionHandleLogin(Context context,String host) {
		Intent intent = new Intent(context, OAuthLogin.class);
		intent.putExtra(EXTRA_INSTANCE, host);
	    context.startActivity(intent);
	}
}
