/*
 * MUSTARD: Android's Client for StatusNet
 * 
 * Copyright (C) 2009-2010 macno.org, Michele Azzolari
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

package org.mustard.android.activity;

import org.mustard.android.MustardApplication;
import org.mustard.android.MustardDbAdapter;
import org.mustard.android.R;
import org.mustard.android.view.RemoteImageView;
import org.mustard.statusnet.Group;
import org.mustard.util.MustardException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MustardGroup extends MustardBaseActivity {
	
	private static final String EXTRA_GROUP = "group";
	private Group mGroup =  null;
	
	public void onCreate(Bundle savedInstanceState) {
		TAG = getClass().getCanonicalName();
		super.onCreate(savedInstanceState);
		
		
		if (mStatusNet != null) {
			
				if (mGroup == null) {
					new AlertDialog.Builder(MustardGroup.this)
					.setTitle(R.string.warning)
					.setMessage(getString(R.string.error_group_not_found,DB_ROW_EXTRA))
					.setNeutralButton(R.string.close, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface xdialog, int id) {
							finish();
						}
					}).show();
				} else {
					showDialog(DIALOG_FETCHING_ID);
					prepareGroupView();
					dismissDialog(DIALOG_FETCHING_ID);
					getStatuses();
				}
		}
	}

	private void prepareGroupView() {
		RemoteImageView mProfileImage = (RemoteImageView) findViewById(R.id.group_image);
		TextView mGroupFullName = (TextView) findViewById(R.id.group_fullname);
		mGroupFullName.setText(mGroup.getFullname());
		
		TextView mGroupInfo = (TextView) findViewById(R.id.group_info);
		mGroupInfo.setText(mGroup.getDescription());

		String profileImageUrl = mGroup.getStream_logo();

		if (profileImageUrl != null && !"".equals(profileImageUrl) && !"null".equalsIgnoreCase(profileImageUrl)) {
			try {
				mProfileImage.setRemoteURI(profileImageUrl);
				mProfileImage.loadImage();
//				MustardApplication.sImageManager.put(profileImageUrl);
//				mProfileImage.setImageBitmap(MustardApplication.sImageManager
//						.get(profileImageUrl));
			} catch (Exception e) {
				if (MustardApplication.DEBUG) Log.e(TAG, "Can't fetch: <"+profileImageUrl+"> " + e.toString());
			}
		}
	}

	@Override
	protected void onBeforeFetch() {
		Intent intent = getIntent();
		DB_ROW_TYPE=MustardDbAdapter.ROWTYPE_GROUP;		
		if (intent.hasExtra(EXTRA_GROUP)) {
			DB_ROW_EXTRA=intent.getExtras().getString(EXTRA_GROUP);			
		} else {
			Uri data = intent.getData();
			DB_ROW_EXTRA=data.getLastPathSegment();
		}
		try {
			mGroup = mStatusNet.getGroup(DB_ROW_EXTRA);
			DB_ROW_EXTRA = mGroup.getNickname();
			
		} catch (MustardException e) {
			e.printStackTrace();
			Log.e(TAG,e.getMessage() == null ? e.toString() : e.getMessage());
		}
		
	}
	

	@Override
	protected void onAfterFetch() {		
	}

	@Override
	protected void onSetListView() {
		if(mLayoutLegacy)
			setContentView(R.layout.legacy_group_list);
		else
			setContentView(R.layout.group_list);
	}
	
	protected void onPreCreateOptionsMenu(Menu menu) {
		menu.add(0, GROUP_JOIN_ID, 0, R.string.menu_join)
		.setIcon(android.R.drawable.ic_menu_add);			
		menu.add(0, GROUP_LEAVE_ID, 0, R.string.menu_leave)
		.setIcon(android.R.drawable.ic_menu_delete);
	}

	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		try {
			boolean isMember = mStatusNet.isGroupMember(DB_ROW_EXTRA);
			menu.findItem(GROUP_JOIN_ID).setVisible(!isMember);
			menu.findItem(GROUP_LEAVE_ID).setVisible(isMember);
		} catch (Exception e) {
			// If group membership request fails
			// Both menu items are shown
			menu.findItem(GROUP_JOIN_ID).setVisible(true);
			menu.findItem(GROUP_LEAVE_ID).setVisible(true);			
		}
		return true;
	}

	public static void actionHandleTimeline(Context context,String group) {
		Intent intent = new Intent(context, MustardGroup.class);
		intent.putExtra(EXTRA_GROUP, group);
	    context.startActivity(intent);
	}
	
}
