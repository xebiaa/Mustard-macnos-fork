/*
 * MUSTARD: Android's Client for StatusNet
 * 
 * Copyright (C) 2009-2010 macno.org, Michele Azzolari
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

package org.mustard.android.activity;

import java.util.Timer;
import java.util.TimerTask;

import org.mustard.android.MustardApplication;
import org.mustard.android.MustardDbAdapter;
import org.mustard.android.Preferences;
import org.mustard.android.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MustardMain extends MustardBaseActivity {

//	static {
//		isMainTimeline=true;
//	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		TAG = getClass().getCanonicalName();
		super.onCreate(savedInstanceState);
		deleteOnExit=false;
		if(mPreferences.getBoolean(Preferences.BOTTOM_BUTTONS, false)) {
			RelativeLayout l_bottom = (RelativeLayout)findViewById(R.id.layout_bottom_bar);
			l_bottom.setVisibility(View.VISIBLE);
			ImageButton ib_compose = (ImageButton)findViewById(R.id.btn_compose_status);
			ib_compose.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					doCompose();
				}
			});
			ImageButton ib_replies = (ImageButton)findViewById(R.id.btn_replies_tl);
			ib_replies.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					getMentions();
				}

			});
			ImageButton ib_refresh = (ImageButton)findViewById(R.id.btn_refresh_status);
			ib_refresh.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					doSilentRefresh();
				}
			});
		}
		if(mStatusNet!=null) {
			mMergedTimeline = mPreferences.getBoolean(Preferences.CHECK_MERGED_TL_KEY, false);
			TextView tagInfo = (TextView) findViewById(R.id.dent_info);
			tagInfo.setText(getString(R.string.timeline_main) + (mMergedTimeline ? " (+) " : ""));

			try {
				fillData();
				doSilentRefresh();
			} catch (Exception e) {
				new AlertDialog.Builder(MustardMain.this)
				.setTitle(getString(R.string.error))
				.setMessage(getString(R.string.error_generic_detail,e.getMessage() == null ? e.toString() : e.getMessage()))
				.setNeutralButton(R.string.close,  new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface xdialog, int id) {
						finish();
					}
				}).show();
			}
		}
		
		mAutoRefresh = mPreferences.getBoolean(Preferences.AUTO_REFRESH_KEY, false);
		if(mAutoRefresh)
			startTimerTask();
	}

	@Override
	protected void onBeforeFetch() {
		DB_ROW_TYPE=MustardDbAdapter.ROWTYPE_FRIENDS;
		if (mMergedTimeline)
			DB_ROW_EXTRA="MERGED";
		else
			DB_ROW_EXTRA=mStatusNet.getMUsername();
	}

	@Override
	protected void onAfterFetch() {		
	}

	@Override
	protected void onSetListView() {
		if(mLayoutLegacy) {
			setContentView(R.layout.legacy_dents_list);
		} else {
			setContentView(R.layout.dents_list);
		}
	}

	public static void actionHandleTimeline(Context context) {
		Intent intent = new Intent(context, MustardMain.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    context.startActivity(intent);
	}

	private Timer t = new Timer();
	
	
	
	private void startTimerTask() {
		
		TimerTask scanTask = new TimerTask() {
			
			@Override
			public void run() {
				
	                mHandler.post(new Runnable() {
	                        public void run() {
	                        	Log.v(TAG, "Timer tick");
	                        	doSilentRefresh();
	                        }
	               });
	        }};

	    String s_delay = mPreferences.getString(Preferences.AUTO_REFRESH_INTERVAL_KEY, getString(R.string.pref_auto_refresh_interval_default));
		long delay = Long.parseLong(s_delay) * 60 * 1000;
	    t.schedule(scanTask, delay, delay); 
	}
	
	
	@Override
	public void onDestroy() {

		super.onDestroy();
		if (MustardApplication.DEBUG) Log.i(TAG,"onDestroy()");

		if (t!=null) {
			t.cancel();
			int i = t.purge();
			if(MustardApplication.DEBUG)
				Log.d(TAG,"Purged " + i + " tasks");
			t = null;
		}

	}
	
}
