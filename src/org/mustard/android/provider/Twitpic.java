package org.mustard.android.provider;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.mustard.android.MustardApplication;
import org.mustard.android.R;
import org.mustard.util.HttpManager;

import android.content.Context;
import android.util.Log;

public class Twitpic {

	public static String upload(Context context, CommonsHttpOAuthConsumer consumer, String message, File media) throws Exception {
		
		String twauth="https://api.twitter.com/1/account/verify_credentials.json";
		String url = consumer.sign(twauth);
		String q = url.substring(url.indexOf("?")+1);
		if (MustardApplication.DEBUG)
			Log.d("Mustard",q);
		String[] qs = q.split("&");
		String xauthserviceprovider="OAuth realm=\"http://api.twitter.com/\"";
		for (int i=0;i<qs.length;i++) {
			String[] qsa = qs[i].split("=");
			xauthserviceprovider +=", "+qsa[0]+"=\""+qsa[1]+"\"";
		}
		if (MustardApplication.DEBUG)
			Log.d("Mustard",xauthserviceprovider);

		HttpManager hm = new HttpManager(null);
		hm.setHost("api.twitpic.com");
		HashMap<String, String> headers = new HashMap<String,String>();
		headers.put("X-Auth-Service-Provider", twauth );
		headers.put("X-Verify-Credentials-Authorization",xauthserviceprovider);
		hm.setExtraHeaders(headers);
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		
		params.add(new BasicNameValuePair("key", context.getString(R.string.twitpic)));
		params.add(new BasicNameValuePair("message",message));
	
		String ret=hm.getResponseAsString("http://api.twitpic.com/2/upload.json", params,"media",media);
		
//		String ret=hm.getResponseAsString("http://michele.cu.mi.it:8080/2/upload.json", params,"media",media);
		
		JSONObject json = new JSONObject(ret);
		String id = json.getString("url");
		return id;
	}

}
